﻿using System;
using System.ComponentModel;
using ECommerceApp.CustomRenderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomSearchBar), typeof(ECommerceApp.iOS.CustomRenderer.CustomSearchBar))]

namespace ECommerceApp.iOS.CustomRenderer
{
    public class CustomSearchBar : SearchBarRenderer
    {
        public CustomSearchBar()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<SearchBar> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.Layer.CornerRadius = 10;
                Control.Layer.BorderWidth = 10;
                Control.Layer.BorderColor = UIColor.Clear.CGColor;
                Control.ShowsCancelButton = false;

                UITextField txSearchField = (UITextField)Control.ValueForKey(new Foundation.NSString("searchField"));
                txSearchField.BackgroundColor = UIColor.FromRGBA(142, 142, 147, 15);
                txSearchField.BorderStyle = UITextBorderStyle.None;
                txSearchField.Layer.BorderWidth = 1.0f;
                txSearchField.Layer.CornerRadius = 10.0f;
                txSearchField.Layer.BorderColor = UIColor.Clear.CGColor;

            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (Control != null)
            {
                Control.ShowsCancelButton = false;
                Control.Layer.CornerRadius = 10.0f;

                UITextField txSearchField = (UITextField)Control.ValueForKey(new Foundation.NSString("searchField"));
                txSearchField.BackgroundColor = UIColor.FromRGBA(142, 142, 147, 15);
                txSearchField.BorderStyle = UITextBorderStyle.None;
                txSearchField.Layer.BorderWidth = 1.0f;
                txSearchField.Layer.CornerRadius = 10.0f;
                txSearchField.Layer.BorderColor = UIColor.Clear.CGColor;

            }
        }
    }
}

