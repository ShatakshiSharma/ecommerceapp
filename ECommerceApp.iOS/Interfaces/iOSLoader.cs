﻿using System;
using BigTed;
using ECommerceApp.Interfaces;

namespace ECommerceApp.iOS.Interfaces
{
    public class iOSLoader : ILoaderInterface
    {
        public void Hide()
        {
            BTProgressHUD.Dismiss();
        }

        public void Show(string title = "")
        {
            BTProgressHUD.Show(title, maskType: ProgressHUD.MaskType.Black);
        }
    }
}
