﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ECommerceApp.Views.Home
{
    public partial class ProductListingPage : ContentPage
    {
        public ProductListingPage()
        {
            InitializeComponent();
        }

        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        void ProductListingCollectionView_SelectionChanged(System.Object sender, Xamarin.Forms.SelectionChangedEventArgs e)
        {
        }
    }
}
