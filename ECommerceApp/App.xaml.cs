﻿using System;
using ECommerceApp.Helpers;
using ECommerceApp.Views;
using ECommerceApp.Views.Home;
using ECommerceApp.Views.LoginSignup;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ECommerceApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            RegisterDependencyServices();
            DependencyService.Get<SettingsHelper>().ReadSettings();

            if (SettingsHelper.IsUserLoggedIn)
            {
                MainPage = new NavPage();
            }
            else
            {
                if (SettingsHelper.IsFirstTimeUser)
                {
                    MainPage = new NavigationPage(new LoginPage());
                }
                else
                {
                    MainPage = new NavigationPage(new FirstLandingPage());
                }
            }
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        private void RegisterDependencyServices()
        {
            DependencyService.Register<SettingsHelper>();
        }
    }
}
