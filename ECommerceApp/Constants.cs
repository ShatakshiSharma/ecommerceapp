﻿using System;

using Xamarin.Forms;

namespace ECommerceApp
{
    public class Constants 
    {
        #region URLs
        public const string APIBaseUrl = "https://dnd.site4demo.com/b2becom/wp-json/";
        public const string APIBaseUrlHTTP = "http://dnd.site4demo.com/b2becom/wp-json/";
        public const string LoginEndpoint = "jwt-auth/v1/token";
        public const string SignUpEndpoint = "update/user";
        public const string CategoryEndpoint = "list/category";
        public const string ProductsEndpoint = "wc/v3/products?category=";
        public const string OTPEndpoint = "otp/login";
        public const string PlaceOrderEndpoint = "create/order";
        public const string OrderListEndpoint = "wc/v3/orders?customer=";

        #endregion

        #region Constant Message
        public static string Somethingwentwrong = "Wrong Username and/or Password. ";
        public static string OK = "OK";
        public const string Error = "Error";
        public const string Success = "Success";

        public static string MobileEmpty = "Please enter mobile number";
        public static string PasswordEmpty = "Please enter password";
        public static string MobileInvalid = "Please enter valid mobile number";
        public static string GSTInvalid = "Please enter valid GST number";
        public const string LoginError = "Login error. Please try again!";
        public static string contactPersonNameEmpty = "Please enter Contact person name";
        public static string EmailEmpty = "Please enter email ID";
        public static string EmailInvalid = "Please enter valid email ID";
        public static string NoDataFound = "No Data Found";
        public static string EmptyCart = "Your cart is empty";
        public const string UnknownError = "Something went wrong. Please try again!";
        public const string OTPError = "Wrong OTP. Please try again!";
        public const string OrderSuccess = "Your order has been successfully placed.";
        public static string DescriptionEmpty = "Please enter description";

        #endregion

        #region APIKey
        public static string username = "username";
        public static string password = "password";
     
        #endregion

    }
}

