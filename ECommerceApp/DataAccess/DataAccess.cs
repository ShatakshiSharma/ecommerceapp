﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using Xamarin.Forms;

namespace ECommerceApp.DataAccess
{
    public class DataAccess
    {
        private static RestClient _restClient = new RestClient(Constants.APIBaseUrl);

        public DataAccess()
        {

        }

        [Obsolete]
        public static async Task<string> GetRequestAsync(string url, Dictionary<string, string> paramsDict = null, Dictionary<string, string> headersDict = null)
        {
            string responceStr = "";
            try
            {
                var request = new RestRequest(url, Method.GET);

                if (paramsDict != null)
                {
                    foreach (var pair in paramsDict)
                    {
                        request.AddParameter(pair.Key, pair.Value);
                    }
                }

                if (headersDict != null)
                {
                    foreach (KeyValuePair<string, string> pair in headersDict)
                    {
                        request.AddHeader(pair.Key, pair.Value);
                    }
                }
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                var cancellationTokenSource = new CancellationTokenSource();

                var restResponse = await _restClient.ExecuteTaskAsync(request, cancellationTokenSource.Token);

                responceStr = restResponse.Content;

                return responceStr;
            }
            catch (Exception exc)
            {
                //DependencyService.Get<Helpers.LoggerHelper>().LogException(exc);
                return exc.Message;
            }
        }

        [Obsolete]
        public static async Task<string> CallURLEncodedPostAPI(string url, Dictionary<string, dynamic> paramsDict = null, Dictionary<string, string> headersDict = null)
        {

            string responceStr = "";
            try
            {

                var request = new RestRequest(url, Method.POST);
                request.Timeout = 30000;

                if (headersDict != null)
                {
                    foreach (var pair in headersDict)
                    {
                        request.AddHeader(pair.Key, pair.Value);
                    }
                }

                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");


                if (paramsDict != null)
                {
                    string urlEncodedParams = "";
                    foreach (var pair in paramsDict)
                    {
                        urlEncodedParams += $"{pair.Key}={pair.Value}&";
                    }

                    urlEncodedParams = urlEncodedParams.Substring(0, urlEncodedParams.Length - 1);

                    request.AddParameter(new Parameter("application/x-www-form-urlencoded", urlEncodedParams, ParameterType.RequestBody));
                }

                var cancellationTokenSource = new CancellationTokenSource();
                var restResponse = await _restClient.ExecuteTaskAsync(request, cancellationTokenSource.Token);

                if (restResponse.StatusCode == System.Net.HttpStatusCode.BadRequest || restResponse.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                {
                     await Application.Current.MainPage.DisplayAlert("", Constants.Somethingwentwrong, Constants.OK);
                }
                responceStr = restResponse.Content;

                return responceStr;
            }
            catch (Exception exc)
            {
                return exc.Message;
            }
        }

    }
}
