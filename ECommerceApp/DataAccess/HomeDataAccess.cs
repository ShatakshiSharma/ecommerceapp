﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ECommerceApp.Models;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace ECommerceApp.DataAccess
{
    public class HomeDataAccess : ContentPage
    {
        public HomeDataAccess()
        {
           
        }

        [Obsolete]
        public static async Task<List<CategoryList>> CallGetCategoryAPI()
        {
            try
            {
                string categoryUrl = Constants.APIBaseUrl + Constants.CategoryEndpoint;

                string jsonStr = await DataAccess.GetRequestAsync(categoryUrl, null);

                var responce = JsonConvert.DeserializeObject<HomeModel>(jsonStr);

                return responce.data;
            }
            catch (Exception exc)
            {
                return new List<CategoryList>();

            }
        }
    }
}

