﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using ECommerceApp.Models;

namespace ECommerceApp.DataAccess
{
    public class LoginDataAccess 
    {
        public LoginDataAccess()
        {
            
        }

        [Obsolete]
        public async Task<bool> CallPostLoginAPI(string Username, string Password)
        {
            bool isLoginSuccessfull = false;

            try
            {
                string loginUrl = Constants.APIBaseUrl + Constants.LoginEndpoint ;

                Dictionary<string, dynamic> dictDict = new Dictionary<string, dynamic>();

                dictDict.Add(Constants.username, Username);
                dictDict.Add(Constants.password, Password);

                string jsonStr = await DataAccess.CallURLEncodedPostAPI(loginUrl, dictDict);

                JObject resultObj = JObject.Parse(jsonStr);

                if (resultObj.SelectToken("code") != null)
                {
                    return isLoginSuccessfull;
                }

                Global.LoggedInUser = JsonConvert.DeserializeObject<LoginModel>(jsonStr);
                Helpers.SettingsHelper.SavedToken = JsonConvert.DeserializeObject<LoginModel>(jsonStr).token;
                isLoginSuccessfull = true;
                return isLoginSuccessfull;
            }
            catch (Exception exc)
            {
                //Helpers.LoggerHelper.LogException(exc);
                return isLoginSuccessfull;
            }
        }

        [Obsolete]
        public async Task<string> CallPostOTPMobileAPI(string Mobile)
        {
            string otp = "";

            try
            {
                string loginUrl = Constants.APIBaseUrl + Constants.OTPEndpoint;

                Dictionary<string, dynamic> dictDict = new Dictionary<string, dynamic>();

                dictDict.Add("mobileno", Mobile);

                string jsonStr = await DataAccess.CallURLEncodedPostAPI(loginUrl, dictDict);
                JObject resultObj = JObject.Parse(jsonStr);

                if (resultObj.SelectToken("otp") != null)
                {
                    otp = (string)resultObj.SelectToken("otp");
                }
                return otp;
            }
            catch (Exception exc)
            {
                //Helpers.LoggerHelper.LogException(exc);
                return otp;
            }
        }
    }

}

