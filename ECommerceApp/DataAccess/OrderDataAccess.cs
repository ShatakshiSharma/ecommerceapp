﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using ECommerceApp.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OAuth;
using Xamarin.Forms;

namespace ECommerceApp.DataAccess
{
    public class OrderDataAccess 
    {
        public OrderDataAccess()
        {
           
        }

        [Obsolete]
        public async Task<bool> CallPostCreateOrderAPI(Dictionary<string, dynamic> dictDict)
        {
            bool isSuccess = false;

            try
            {
                string orderURL = Constants.APIBaseUrl + Constants.PlaceOrderEndpoint;

                string jsonStr = await DataAccess.CallURLEncodedPostAPI(orderURL, dictDict);
                JObject resultObj = JObject.Parse(jsonStr);

                if (resultObj.SelectToken("code") != null)
                {
                    return isSuccess;
                }
                isSuccess = true;
                return isSuccess;
            }
            catch (Exception exc)
            {
                //Helpers.LoggerHelper.LogException(exc);
                return isSuccess;
            }
        }

        [Obsolete]
        public static async Task<List<OrderArray>> CallGetOrderListAPI()
        {
            try
            {
                string orderUrl = Constants.APIBaseUrl + Constants.OrderListEndpoint + Global.UserDetail.data.id;

                string url2, param;

                var uri = new Uri(orderUrl);
                OAuthBase oAuth = new OAuthBase();
                var signature = HttpUtility.UrlEncode(oAuth.GenerateSignature(uri, "ck_902cc668d419a874046ffd9aaaa89add77c86b45", "cs_ddcf37da10abcc576ec8a648aae48a8189485d45", null, null, "GET", oAuth.GenerateTimeStamp(), oAuth.GenerateNonce(), OAuthBase.SignatureTypes.HMACSHA1, out url2, out param));

                url2 = url2 + "?" + param + "&oauth_signature=" + signature;

                string jsonStr = await DataAccess.GetRequestAsync(url2, null);

                var responce = JsonConvert.DeserializeObject<List<OrderArray>>(jsonStr);

                return responce;
            }
            catch (Exception exc)
            {
                return new List<OrderArray>();

            }
        }
    }
}

