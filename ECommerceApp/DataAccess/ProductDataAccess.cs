﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using ECommerceApp.Models;
using Newtonsoft.Json;
using OAuth;
using Xamarin.Forms;

namespace ECommerceApp.DataAccess
{
    public class ProductDataAccess : ContentPage
    {
        public ProductDataAccess()
        {
            
        }

        [Obsolete]
        public static async Task<List<ProductDetailModel>> CallGetProductListAPI(string id)
        {
            try
            {
                string categoryProductUrl = Constants.APIBaseUrl + Constants.ProductsEndpoint + id;
                string url2, param;

                var uri = new Uri(categoryProductUrl);
                OAuthBase oAuth = new OAuthBase();
                var signature = HttpUtility.UrlEncode(oAuth.GenerateSignature(uri, "ck_902cc668d419a874046ffd9aaaa89add77c86b45", "cs_ddcf37da10abcc576ec8a648aae48a8189485d45", null, null, "GET", oAuth.GenerateTimeStamp(), oAuth.GenerateNonce(), OAuthBase.SignatureTypes.HMACSHA1, out url2, out param));

                url2 = url2 + "?"+ param + "&oauth_signature=" + signature ;

                string jsonStr = await DataAccess.GetRequestAsync(url2, null, null);

                var responce = JsonConvert.DeserializeObject<List<ProductDetailModel>>(jsonStr);

                return responce;
            }
            catch (Exception exc)
            {
                return new List<ProductDetailModel>();

            }
        }
    }
}

