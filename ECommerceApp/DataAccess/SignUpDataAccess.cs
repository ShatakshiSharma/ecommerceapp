﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ECommerceApp.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace ECommerceApp.DataAccess
{
    public class SignUpDataAccess 
    {
        public SignUpDataAccess()
        {
            
        }

        [Obsolete]
        public async Task<bool> CallPostUpdateUserAPI(Dictionary<string, dynamic> parameters)
        {
            bool isSignUpSuccessfull = false;

            try
            {
                string signupUrl = Constants.APIBaseUrl + Constants.SignUpEndpoint;

                string jsonStr = await DataAccess.CallURLEncodedPostAPI(signupUrl, parameters);

                JObject resultObj = JObject.Parse(jsonStr);

                if (resultObj.SelectToken("code") != null)
                {
                    return isSignUpSuccessfull;
                }
                Global.UserDetail = JsonConvert.DeserializeObject<SignUpModel>(jsonStr);
                Helpers.SettingsHelper.IsUserLoggedIn = true;

                isSignUpSuccessfull = true;
                return isSignUpSuccessfull;
            }
            catch (Exception exc)
            {
                //Helpers.LoggerHelper.LogException(exc);
                return isSignUpSuccessfull;
            }
        }
    }
}

