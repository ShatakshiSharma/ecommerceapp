﻿using System;
using System.Collections.Generic;
using ECommerceApp.Helpers;
using ECommerceApp.Models;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace ECommerceApp
{
    public class Global 
    {
        public Global()
        {
        }

        private static LoginModel _loggedInUser;
        public static LoginModel LoggedInUser
        {
            get => _loggedInUser;
            set
            {
                _loggedInUser = value;
                if (_loggedInUser != null)
                {
                    Helpers.SettingsHelper.LoggedInUser = JsonConvert.SerializeObject(_loggedInUser);
                }
            }
        }

        private static SignUpModel _userDetail;
        public static SignUpModel UserDetail
        {
            get => _userDetail;
            set
            {
                _userDetail = value;
                if (_userDetail != null)
                {
                    Helpers.SettingsHelper.UserDetail = JsonConvert.SerializeObject(_userDetail);
                }
            }
        }

        private static List<MyCartModel> _cart;
        public static List<MyCartModel> Cart
        {
            get => _cart;
            set
            {
                _cart = value;
                if (_cart != null)
                {
                    Helpers.SettingsHelper.Cart = JsonConvert.SerializeObject(_cart);
                }
            }
        }

        public static NavigationPage page1 { get; set; }
        public static NavigationPage page2 { get; set; }
        public static NavigationPage page3 { get; set; }
        public static NavigationPage page4 { get; set; }

    }
}

