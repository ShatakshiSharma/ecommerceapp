﻿using System;
using System.Collections.Generic;
using ECommerceApp.Models;
using Newtonsoft.Json;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace ECommerceApp.Helpers
{
    public class SettingsHelper 
    {
        public static bool IsUserLoggedIn
        {
            get => Preferences.Get(nameof(IsUserLoggedIn), false);
            set => Preferences.Set(nameof(IsUserLoggedIn), value);
        }

        public static bool IsFirstTimeUser
        {
            get => Preferences.Get(nameof(IsFirstTimeUser), false);
            set => Preferences.Set(nameof(IsFirstTimeUser), value);
        }

        public static string LoggedInUser
        {
            get => Preferences.Get(nameof(LoggedInUser), "");
            set => Preferences.Set(nameof(LoggedInUser), value);
        }

        public static string UserDetail
        {
            get => Preferences.Get(nameof(UserDetail), "");
            set => Preferences.Set(nameof(UserDetail), value);
        }

        public static string Cart
        {
            get => Preferences.Get(nameof(Cart), "");
            set => Preferences.Set(nameof(Cart), value);
        }

        public static string SavedToken
        {
            get => Preferences.Get(nameof(SavedToken), "");
            set => Preferences.Set(nameof(SavedToken), value);
        }

        public SettingsHelper()
        {

        }

        public void ReadSettings()
        {
            try
            {
                if (IsUserLoggedIn || IsFirstTimeUser)
                {
                    Global.LoggedInUser = JsonConvert.DeserializeObject<LoginModel>(LoggedInUser);
                    Global.UserDetail = JsonConvert.DeserializeObject<SignUpModel>(UserDetail);
                    Global.Cart = JsonConvert.DeserializeObject<List<MyCartModel>>(Cart);
                }
            }
            catch (Exception exc)
            {
                //DependencyService.Get<Helpers.LoggerHelper>().LogException(exc);
            }
        }

        public void ClearAll()
        {
            Preferences.Clear();
        }

        public void ClearCart()
        {
            Preferences.Remove(nameof(Cart));
        }

        public void Logout()
        {
            Preferences.Remove(nameof(IsUserLoggedIn));
        }
    }
}

