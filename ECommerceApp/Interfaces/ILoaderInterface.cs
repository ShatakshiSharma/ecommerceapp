﻿using System;
namespace ECommerceApp.Interfaces
{
    public interface ILoaderInterface
    {
        /// <summary>
        /// Shows the specified title.
        /// </summary>
        /// <param name="title">The title.</param>
        void Show(string title = "");
        /// <summary>
        /// Hides this instance.
        /// </summary>
        void Hide();
    }
}
