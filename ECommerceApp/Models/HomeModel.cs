﻿using System;
using System.Collections.Generic;
using ECommerceApp.ViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Models
{
    public class HomeModel : BaseViewModel
    {
        public string status { get; set; }
        public string statusCode { get; set; }
        public List<CategoryList> data { get; set; }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class CategoryList
    {
        public string id { get; set; }
        public string name { get; set; }
        public string slug { get; set; }
        public string parent { get; set; }
        public string description { get; set; }
        public string count { get; set; }
        public string menu_order { get; set; }
        public object image { get; set; }
    }

}

