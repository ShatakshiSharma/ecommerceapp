﻿using System;
using ECommerceApp.ViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Models
{
    public class LoginModel : BaseViewModel
    {
        public string token { get; set; }
        public string user_email { get; set; }
        public string user_nicename { get; set; }
        public string user_display_name { get; set; }
        public string firm_name { get; set; }
        public string mobile_number { get; set; }
    }
}

