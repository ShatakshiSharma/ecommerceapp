﻿using System;
using ECommerceApp.ViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Models
{
    public class MyCartModel : BaseViewModel
    {
        public string product_name { get; set; }
        public string product_id { get; set; }
        public string image { get; set; }
        public string quantity { get; set; }
        public string displayQuantity { get; set; }
        public string price { get; set; }
        public string displayPrice { get; set; }
        public string total { get; set; }
        public string displayTotal { get; set; }
    }
}

