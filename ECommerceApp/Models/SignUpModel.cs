﻿using System;
using System.Collections.Generic;
using ECommerceApp.ViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Models
{
    public class SignUpModel : BaseViewModel
    {
        public string status { get; set; }
        public string statusCode { get; set; }
        public Data data { get; set; }
    }

    public class Data
    {
        public int id { get; set; }
        public DateTime date_created { get; set; }
        public DateTime date_created_gmt { get; set; }
        public DateTime date_modified { get; set; }
        public DateTime date_modified_gmt { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string role { get; set; }
        public string username { get; set; }
        public Billing billing { get; set; }
        public Shipping shipping { get; set; }
        public bool is_paying_customer { get; set; }
        public string avatar_url { get; set; }
        public List<MetaData> meta_data { get; set; }
        public Links _links { get; set; }
    }
   
}

