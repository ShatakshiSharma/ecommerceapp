﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ECommerceApp.DataAccess;
using ECommerceApp.Interfaces;
using ECommerceApp.Models;
using ECommerceApp.Views.Home;
using Xamarin.Forms;

namespace ECommerceApp.ViewModels.HomeViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        private HomeDataAccess _homeDataAccess = new HomeDataAccess();

        private List<CategoryList> _categoryList = new List<CategoryList>();
        public List<CategoryList> CategoryList
        {
            get { return _categoryList; }
            set
            {
                _categoryList = value;
                OnPropertyChanged(nameof(CategoryList));
            }
        }

        private List<CategoryList> _completeCategoryList = new List<CategoryList>();
        public List<CategoryList> CompleteCategoryList
        {
            get { return _completeCategoryList; }
            set
            {
                _completeCategoryList = value;
                OnPropertyChanged(nameof(CompleteCategoryList));
            }
        }

        private List<CategoryList> _subCategoryList = new List<CategoryList>();
        public List<CategoryList> SubCategoryList
        {
            get { return _subCategoryList; }
            set
            {
                _subCategoryList = value;
                OnPropertyChanged(nameof(SubCategoryList));
            }
        }

        private bool _collectionViewIsVisible = true;
        public bool CollectionViewIsVisible
        {
            get { return _collectionViewIsVisible; }
            set { SetProperty(ref _collectionViewIsVisible, value); }
        }

        private bool _labelIsVisible = false;
        public bool LabelIsVisible
        {
            get { return _labelIsVisible; }
            set { SetProperty(ref _labelIsVisible, value); }
        }

        private string _labelMessage = Constants.NoDataFound;
        public string LabelMessage
        {
            get { return _labelMessage; }
            set { SetProperty(ref _labelMessage, value); }
        }

        public HomeViewModel()
        {

        }

        public async Task GetcategoryListAsync()
        {
            try
            {
                await Task.Delay(1000);

                DependencyService.Get<ILoaderInterface>().Show("Wait . . .");
                CompleteCategoryList = await HomeDataAccess.CallGetCategoryAPI();
                var NewList = new List<CategoryList>();
                SubCategoryList = new List<CategoryList>();

                foreach (CategoryList cat in CompleteCategoryList)
                {
                    if (cat.parent == "0"){
                        NewList.Add(cat);
                    }
                }
                CategoryList = NewList;
                if (CategoryList == null || CategoryList.Count == 0)
                {
                    CollectionViewIsVisible = false;
                    LabelIsVisible = true;
                }
                else
                {
                    CollectionViewIsVisible = true;
                    LabelIsVisible = false;
                }
                OnPropertyChanged(nameof(CategoryList));
                DependencyService.Get<ILoaderInterface>().Hide();
            }
            catch (Exception exc)
            {
                // DependencyService.Get<Helpers.LoggerHelper>().LogException(exc);
            }
        }

        public async Task GoToSubCategoryScreen(List<CategoryList> subcategory, string title)
        {
            OnPropertyChanged(nameof(SubCategoryList));
            SubCategoryPage obj = new SubCategoryPage(subcategory, title);
            await Xamarin.Forms.Application.Current.MainPage.Navigation.PushModalAsync(obj);
        }

        public async Task GoToProductListingScreen(string id, string title)
        {
            ProductListingPage obj = new ProductListingPage(id, title);
            await Xamarin.Forms.Application.Current.MainPage.Navigation.PushModalAsync(obj);
        }
    }
}

