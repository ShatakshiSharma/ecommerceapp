﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using ECommerceApp.Interfaces;
using ECommerceApp.Models;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace ECommerceApp.ViewModels.HomeViewModels
{
    public class ProductDetailViewModel : BaseViewModel
    {
        public ICommand AddToCartCommand { get; set; }

        private ProductDetailModel _productList = new ProductDetailModel();
        public ProductDetailModel ProductList
        {
            get { return _productList; }
            set
            {
                _productList = value;
                OnPropertyChanged(nameof(ProductList));
            }
        }

        private string _quantity = "";
        public string Quantity
        {
            get { return _quantity; }
            set { SetProperty(ref _quantity, value); }
        }

        private string _quantityText = "Add Quantity";
        public string QuantityText
        {
            get { return _quantityText; }
            set { SetProperty(ref _quantityText, value); }
        }

        private string _brand = "";
        public string Brand
        {
            get { return _brand; }
            set { SetProperty(ref _brand, value); }
        }

        private string _buttonColor = "#D0D0D0";
        public string ButtonColor
        {
            get { return _buttonColor; }
            set { SetProperty(ref _buttonColor, value); }
        }

        private List<string> _colorList = new List<string>();
        public List<string> ColorList
        {
            get { return _colorList; }
            set { SetProperty(ref _colorList, value); }
        }

        private List<MyCartModel> _cartList = new List<MyCartModel>();
        public List<MyCartModel> CartList
        {
            get { return _cartList; }
            set { SetProperty(ref _cartList, value); }
        }

        public ProductDetailViewModel()
        {
            AddToCartCommand = new Command(AddToCartAction);
        }

        public async Task AddQuantity()
        {
            if (Quantity != null && Quantity != "" && Quantity!= "0")
            {
                QuantityText = Quantity + " Quantity";
                ButtonColor = "Red";

                OnPropertyChanged(nameof(QuantityText));
                OnPropertyChanged(nameof(ButtonColor));
            }
        }

        public async void AddToCartAction()
        {
            if (Quantity != null && Quantity != "" && Quantity != "0")
            {
                if (Global.Cart != null)
                {
                    CartList = Global.Cart;
                }
                MyCartModel cartModel = new MyCartModel();
                cartModel.product_id = "1088";
                cartModel.product_name = Title;
                cartModel.quantity = Quantity;
                cartModel.displayQuantity = "Qty: " + Quantity;
                cartModel.price = ProductList.Price;
                cartModel.displayPrice = ProductList.DisplayPrice;
                cartModel.image = ProductList.DisplayImage;
                cartModel.total = (int.Parse(Quantity) * int.Parse(ProductList.Price)).ToString();

                foreach ( var product in CartList)
                {
                    cartModel.total = (int.Parse(cartModel.total) + int.Parse(product.total)).ToString();
                }
                cartModel.displayTotal = "₹" + cartModel.total;

                CartList.Add(cartModel);

                Helpers.SettingsHelper.Cart = JsonConvert.SerializeObject(CartList);
                Global.Cart = JsonConvert.DeserializeObject<List<MyCartModel>>(Helpers.SettingsHelper.Cart);

                await Application.Current.MainPage.DisplayAlert(Constants.Success, "Product added to cart.", Constants.OK);

                OnPropertyChanged(nameof(CartList));
            }
        }

        public async Task GetProductDetailtAsync()
        {
            try
            {
                foreach (var attribute in ProductList.Attributes)
                {
                    if (attribute.Name == "Color")
                    {
                        ColorList = attribute.Options;
                    }
                }
                foreach (var brand in ProductList.Brands)
                {
                    Brand = brand.Name;
                    return;
                }

                OnPropertyChanged(nameof(ColorList));
                OnPropertyChanged(nameof(Brand));

            }
            catch (Exception exc)
            {
                // DependencyService.Get<Helpers.LoggerHelper>().LogException(exc);
            }
        }
    }
}

