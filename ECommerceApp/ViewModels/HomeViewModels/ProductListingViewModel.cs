﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ECommerceApp.DataAccess;
using ECommerceApp.Interfaces;
using ECommerceApp.Models;
using ECommerceApp.Views.Home;
using Xamarin.Forms;
using Image = ECommerceApp.Models.Image;

namespace ECommerceApp.ViewModels.HomeViewModels
{
    public class ProductListingViewModel : BaseViewModel
    {
        private string _id = "";
        public string ID
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        private List<ProductDetailModel> _productList = new List<ProductDetailModel>();
        public List<ProductDetailModel> ProductList
        {
            get { return _productList; }
            set { SetProperty(ref _productList, value); }
        }

        private bool _collectionViewIsVisible = true;
        public bool CollectionViewIsVisible
        {
            get { return _collectionViewIsVisible; }
            set { SetProperty(ref _collectionViewIsVisible, value); }
        }

        private bool _labelIsVisible = false;
        public bool LabelIsVisible
        {
            get { return _labelIsVisible; }
            set { SetProperty(ref _labelIsVisible, value); }
        }

        private string _labelMessage = Constants.NoDataFound;
        public string LabelMessage
        {
            get { return _labelMessage; }
            set { SetProperty(ref _labelMessage, value); }
        }

        private string _image = "";
        public string Image
        {
            get { return _image; }
            set { SetProperty(ref _image, value); }
        }

        public ProductListingViewModel()
        {
        }

        [Obsolete]
        public async Task GetProductListAsync()
        {
            try
            {
                DependencyService.Get<ILoaderInterface>().Show("Wait . . .");

                ProductList = await ProductDataAccess.CallGetProductListAPI(ID);

                if (ProductList != null && ProductList.Count > 0)
                {
                    CollectionViewIsVisible = true;
                    LabelIsVisible = false;

                    foreach (var product in ProductList)
                    {
                        product.ShortDescription = RemoveHTMLTags(product.ShortDescription);
                        product.Description = RemoveHTMLTags(product.Description);

                        List<Image> Images = product.Images;
                        if (Images.Count > 0)
                        {
                            product.DisplayImage = Images[0].Src.ToString();
                        }
                    }
                }
                else
                {
                    CollectionViewIsVisible = false;
                    LabelIsVisible = true;
                }

                OnPropertyChanged(nameof(ProductList));

                DependencyService.Get<ILoaderInterface>().Hide();
            }
            catch (Exception exc)
            {
                // DependencyService.Get<Helpers.LoggerHelper>().LogException(exc);
            }
        }

        public string RemoveHTMLTags(string HTMLCode)
        {
            return System.Text.RegularExpressions.Regex.Replace(
              HTMLCode, "<[^>]*>", "");
        }

        public async Task GoToProductDetailScreen(ProductDetailModel detailModel)
        {
            ProductDetailPage obj = new ProductDetailPage(detailModel);
            await Application.Current.MainPage.Navigation.PushModalAsync(obj);
        }
    }
}

