﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using ECommerceApp.DataAccess;
using ECommerceApp.Interfaces;
using ECommerceApp.Views;
using ECommerceApp.Views.LoginSignup;
using Xamarin.Forms;

namespace ECommerceApp.ViewModels
{
    public class FirstlandingPageViewModel : BaseViewModel
    {
        public ICommand SubmitCommand { get; set; }
        private string mobile = "";
        private string password = "";
        private LoginDataAccess _loginDataAccess = new LoginDataAccess();

        public string Mobile
        {
            get
            {
                return mobile;
            }
            set
            {
                SetProperty(ref mobile, value);
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                SetProperty(ref password, value);
            }
        }
        public FirstlandingPageViewModel()
        {
            SubmitCommand = new Command(SubmitAction);
        }

        [Obsolete]
        private async void SubmitAction()
        {
            await LoginAsync();
        }

        [Obsolete]
        public async Task LoginAsync()
        {
            if (await ValidateFieldsAsync())
            {
                try
                {
                    DependencyService.Get<ILoaderInterface>().Show("Wait . . .");
                    bool isSuccess = await _loginDataAccess.CallPostLoginAPI(Mobile, Password);
                    DependencyService.Get<ILoaderInterface>().Hide();
                    if (isSuccess)
                    {
                        Helpers.SettingsHelper.IsUserLoggedIn = true;
                        Helpers.SettingsHelper.IsFirstTimeUser = true;
                        Mobile = "";
                        Password = "";
                        SignUpPage otpObj = new SignUpPage();
                        await Xamarin.Forms.Application.Current.MainPage.Navigation.PushModalAsync(otpObj);
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.LoginError, Constants.OK);
                    }
                }
                catch (Exception exc)
                {
                   // DependencyService.Get<Helpers.LoggerHelper>().LogException(exc);
                }
            }
        }

        private async Task<bool> ValidateFieldsAsync()
        {
            bool success = true;

            if (string.IsNullOrEmpty(Mobile))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.MobileEmpty, Constants.OK);
                success = false;
            }
            else if (string.IsNullOrEmpty(Password))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.PasswordEmpty, Constants.OK);
                success = false;
            }
            else if (!Regex.IsMatch(Mobile, @"^[0-9]{10}$", RegexOptions.IgnoreCase))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.MobileInvalid, Constants.OK);
                success = false;
            }

            return success;
        }
    }
}
