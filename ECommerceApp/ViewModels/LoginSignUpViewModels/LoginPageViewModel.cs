﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using ECommerceApp.Views;
using ECommerceApp.Views.LoginSignup;
using System.Threading.Tasks;
using ECommerceApp.Interfaces;
using ECommerceApp.DataAccess;
using System.Text.RegularExpressions;
using Xamarin.Forms.Xaml;

namespace ECommerceApp.ViewModels
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public class LoginPageViewModel : BaseViewModel
    {
        public ICommand LoginCommand { get; set; }
        private LoginDataAccess _loginDataAccess = new LoginDataAccess();

        private string mobile = "";
        public string Mobile
        {
            get
            {
                return mobile;
            }
            set
            {
                SetProperty(ref mobile, value);
            }
        }

        public LoginPageViewModel()
        {
            LoginCommand = new Command(LoginAction);
        }

        private async void LoginAction()
        {
            await SendOTPAsync();
        }

        [Obsolete]
        public async Task SendOTPAsync()
        {
            if (!string.IsNullOrEmpty(Mobile))
            {
                if (Regex.IsMatch(Mobile, @"^[0-9]{10}$", RegexOptions.IgnoreCase))
                {
                    try
                    {
                        DependencyService.Get<ILoaderInterface>().Show("Wait . . .");
                        string otp = await _loginDataAccess.CallPostOTPMobileAPI(Mobile);
                        DependencyService.Get<ILoaderInterface>().Hide();
                        if (!string.IsNullOrEmpty(Mobile))
                        {
                            OtpVerificationPage obj = new OtpVerificationPage(otp);
                            await Xamarin.Forms.Application.Current.MainPage.Navigation.PushModalAsync(obj);

                            //Application.Current.MainPage = new NavigationPage(new SignUpPage());
                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.UnknownError, Constants.OK);
                        }
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc);
                        // DependencyService.Get<Helpers.LoggerHelper>().LogException(exc);
                    }

                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.MobileInvalid, Constants.OK);
                }
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.MobileEmpty, Constants.OK);
            }
        }
    }
}
