﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using ECommerceApp.DataAccess;
using ECommerceApp.Helpers;
using ECommerceApp.Interfaces;
using ECommerceApp.Views;
using ECommerceApp.Views.LoginSignup;
using Xamarin.Forms;

namespace ECommerceApp.ViewModels.LoginSignUpViewModels
{
    public class OtpVerificationPageViewModel : BaseViewModel
    {
        public ICommand SubmitCommand { get; set; }
        public ICommand LoginCommand { get; set; }

        private OrderDataAccess _orderDataAccess = new OrderDataAccess();

        private string _mobile = "Enter OTP sent to " + Global.LoggedInUser.mobile_number;
        public string Mobile
        {
            get { return _mobile; }
            set { SetProperty(ref _mobile, value); }
        }

        private string _OTP = "";
        public string OTP
        {
            get { return _OTP; }
            set { SetProperty(ref _OTP, value); }
        }

        private string _OTPResponse = "";
        public string OTPResponse
        {
            get { return _OTPResponse; }
            set { SetProperty(ref _OTPResponse, value); }
        }

        private List<Dictionary<string, string>> _CartArray = new List<Dictionary<string, string>>();
        public List<Dictionary<string, string>> CartArray
        {
            get { return _CartArray; }
            set { SetProperty(ref _CartArray, value); }
        }

        public OtpVerificationPageViewModel(bool isLogin)
        {
            //SubmitCommand = new Command(SubmitAction);
            //LoginCommand = new Command(LoginAction);

            if (!isLogin)
            {
                LoginCommand = new Command(SubmitAction);
            }
            else
            {
                LoginCommand = new Command(LoginAction);
            }
        }

        private async void LoginAction()
        {
            if (OTP == OTPResponse)
            {
                Application.Current.MainPage = new NavPage();
                Helpers.SettingsHelper.IsUserLoggedIn = true;
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.OTPError, Constants.OK);
            }
        }

        private async void SubmitAction()
        {
            if (OTP == OTPResponse)
            {
                await CreateOrderAsync();
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.OTPError, Constants.OK);
            }
        }

        [Obsolete]
        public async Task CreateOrderAsync()
        {
            try
            {
                DependencyService.Get<ILoaderInterface>().Show("Wait . . .");

                Dictionary<string, string> billingDict = new Dictionary<string, string>();
                billingDict.Add("first_name", Global.UserDetail.data.first_name);
                billingDict.Add("last_name", Global.UserDetail.data.last_name);
                billingDict.Add("company", Global.UserDetail.data.billing.company);
                billingDict.Add("country", Global.UserDetail.data.billing.country);
                billingDict.Add("phone", Global.LoggedInUser.mobile_number);
                billingDict.Add("address_1", Global.UserDetail.data.billing.address_1);
                billingDict.Add("address_2", Global.UserDetail.data.billing.address_2);
                billingDict.Add("state", Global.UserDetail.data.billing.state);
                billingDict.Add("city", Global.UserDetail.data.billing.city);
                billingDict.Add("postcode", Global.UserDetail.data.billing.postcode);

                Dictionary<string, string> shippingDict = new Dictionary<string, string>();
                shippingDict.Add("address_1", Global.UserDetail.data.shipping.address_1);
                shippingDict.Add("address_2", Global.UserDetail.data.shipping.address_2);
                shippingDict.Add("state", Global.UserDetail.data.shipping.state);
                shippingDict.Add("city", Global.UserDetail.data.shipping.city);
                shippingDict.Add("postcode", Global.UserDetail.data.shipping.postcode);
                shippingDict.Add("first_name", Global.UserDetail.data.first_name);
                shippingDict.Add("last_name", Global.UserDetail.data.last_name);
                shippingDict.Add("company", Global.UserDetail.data.shipping.company);
                shippingDict.Add("country", Global.UserDetail.data.shipping.country);
                shippingDict.Add("phone", Global.LoggedInUser.mobile_number);

                Dictionary<string, dynamic> dictDict = new Dictionary<string, dynamic>();
                dictDict.Add("mobile_number", Global.LoggedInUser.mobile_number);
                dictDict.Add("billing", billingDict);
                dictDict.Add("shipping", shippingDict);
                dictDict.Add("products", CartArray);

                bool isSuccess = await _orderDataAccess.CallPostCreateOrderAPI(dictDict);
                DependencyService.Get<ILoaderInterface>().Hide();
                if (isSuccess)
                {
                    await Application.Current.MainPage.DisplayAlert(Constants.Success, Constants.OrderSuccess, Constants.OK);
                    DependencyService.Get<SettingsHelper>().ClearCart();
                    DependencyService.Get<SettingsHelper>().ReadSettings();
                    Application.Current.MainPage = new NavPage();
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.UnknownError, Constants.OK);
                }
            }
            catch (Exception exc)
            {
                DependencyService.Get<ILoaderInterface>().Hide();

                // DependencyService.Get<Helpers.LoggerHelper>().LogException(exc);
            }
        }
    }
}

