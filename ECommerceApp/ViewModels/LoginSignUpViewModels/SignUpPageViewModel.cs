﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using ECommerceApp.DataAccess;
using ECommerceApp.Helpers;
using ECommerceApp.Interfaces;
using ECommerceApp.Views;
using ECommerceApp.Views.Home;
using ECommerceApp.Views.LoginSignup;
using RestSharp;
using Xamarin.Forms;

namespace ECommerceApp.ViewModels.LoginSignUpViewModels
{
    public class SignUpPageViewModel : BaseViewModel
    {
        public ICommand SubmitCommand { get; set; }
        public ICommand ShippingAddressCommand { get; set; }
        public ICommand TermsConditionsCommand { get; set; }
        public ICommand PushbackCommand { get; set; }

        private string firmName = Global.LoggedInUser.firm_name;
        private string contactPersonName = "";
        private string mobile = Global.LoggedInUser.mobile_number;
        private string city = "";
        private string email = "";
        private string billingAddress = "";
        private string billingArea = "";
        private string billingState = "";
        private string billingCity = "";
        private string billingPincode = "";
        private string billingGST = "";
        private string shippingAddress = "";
        private string shippingArea = "";
        private string shippingState = "";
        private string shippingCity = "";
        private string shippingPincode = "";

        public string FirmName
        {
            get
            {
                return firmName;
            }
            set
            {
                SetProperty(ref firmName, value);
            }
        }

        public string ContactPersonName
        {
            get
            {
                return contactPersonName;
            }
            set
            {
                SetProperty(ref contactPersonName, value);
            }
        }

        public string Mobile
        {
            get
            {
                return mobile;
            }
            set
            {
                SetProperty(ref mobile, value);
            }
        }

        public string City
        {
            get
            {
                return city;
            }
            set
            {
                SetProperty(ref city, value);
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                SetProperty(ref email, value);
            }
        }

        public string BillingAddress
        {
            get
            {
                return billingAddress;
            }
            set
            {
                SetProperty(ref billingAddress, value);
            }
        }

        public string BillingArea
        {
            get
            {
                return billingArea;
            }
            set
            {
                SetProperty(ref billingArea, value);
            }
        }

        public string BillingState
        {
            get
            {
                return billingState;
            }
            set
            {
                SetProperty(ref billingState, value);
            }
        }

        public string BillingCity
        {
            get
            {
                return billingCity;
            }
            set
            {
                SetProperty(ref billingCity, value);
            }
        }

        public string BillingPincode
        {
            get
            {
                return billingPincode;
            }
            set
            {
                SetProperty(ref billingPincode, value);
            }
        }

        public string BillingGST
        {
            get
            {
                return billingGST;
            }
            set
            {
                SetProperty(ref billingGST, value);
            }
        }

        public string ShippingAddress
        {
            get
            {
                return shippingAddress;
            }
            set
            {
                SetProperty(ref shippingAddress, value);
            }
        }

        public string ShippingArea
        {
            get
            {
                return shippingArea;
            }
            set
            {
                SetProperty(ref shippingArea, value);
            }
        }

        public string ShippingState
        {
            get
            {
                return shippingState;
            }
            set
            {
                SetProperty(ref shippingState, value);
            }
        }

        public string ShippingCity
        {
            get
            {
                return shippingCity;
            }
            set
            {
                SetProperty(ref shippingCity, value);
            }
        }

        public string ShippingPincode
        {
            get
            {
                return shippingPincode;
            }
            set
            {
                SetProperty(ref shippingPincode, value);
            }
        }

        private SignUpDataAccess _signUpDataAccess = new SignUpDataAccess();

        public SignUpPageViewModel()
        {
            SubmitCommand = new Command(SubmitAction);
            ShippingAddressCommand = new Command(AddShippingAddressAction);
            TermsConditionsCommand = new Command(TermsConditionsAction);
            PushbackCommand = new Command(PushBack);
        }

        [Obsolete]
        private async void SubmitAction()
        {
            await SignUpAsync();
        }

        private async void AddShippingAddressAction()
        {
            ShippingAddresspage otpObj = new ShippingAddresspage();
            await Xamarin.Forms.Application.Current.MainPage.Navigation.PushModalAsync(otpObj);
        }

        private async void TermsConditionsAction()
        {
            TermsAndConditionsPage otpObj = new TermsAndConditionsPage();
            await Xamarin.Forms.Application.Current.MainPage.Navigation.PushModalAsync(otpObj);
        }

        private async void PushBack()
        {
            await Xamarin.Forms.Application.Current.MainPage.Navigation.PopModalAsync();
        }

        [Obsolete]
        public async Task SignUpAsync()
        {
            if (await ValidateFieldsAsync())
            {
                try
                {
                    DependencyService.Get<ILoaderInterface>().Show("Wait . . .");

                    Dictionary<string, object> billingDict = new Dictionary<string, object>();
                    billingDict.Add("address_1", BillingAddress);
                    billingDict.Add("address_2", BillingArea);
                    billingDict.Add("state", BillingState);
                    billingDict.Add("city", BillingCity);
                    billingDict.Add("postcode", BillingPincode);

                    Dictionary<string, object> shippingDict = new Dictionary<string, object>();
                    shippingDict.Add("address_1", BillingAddress);
                    shippingDict.Add("address_2", BillingArea);
                    shippingDict.Add("state", BillingState);
                    shippingDict.Add("city", BillingCity);
                    shippingDict.Add("postcode", BillingPincode);

                    JsonObject billingObject = new JsonObject();
                    foreach (var kvp in billingDict)
                    {
                        billingObject.Add(kvp);
                    }

                    JsonObject shippingObject = new JsonObject();
                    foreach (var kvp in shippingDict)
                    {
                        shippingObject.Add(kvp);
                    }

                    Dictionary<string, dynamic> dictDict = new Dictionary<string, dynamic>();
                    dictDict.Add("token", Helpers.SettingsHelper.SavedToken);
                    dictDict.Add("name", ContactPersonName);
                    dictDict.Add("email", Email);
                    dictDict.Add("gst_number", BillingGST);
                    dictDict.Add("billing", billingObject.ToString());
                    dictDict.Add("shipping", shippingObject.ToString());

                    bool isSuccess = await _signUpDataAccess.CallPostUpdateUserAPI(dictDict);
                    DependencyService.Get<ILoaderInterface>().Hide();
                    if (isSuccess)
                    {
                        Helpers.SettingsHelper.IsUserLoggedIn = true;
                        Application.Current.MainPage = new NavPage();
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.UnknownError, Constants.OK);
                    }
                }
                catch (Exception exc)
                {
                    DependencyService.Get<ILoaderInterface>().Hide();

                    // DependencyService.Get<Helpers.LoggerHelper>().LogException(exc);
                }
            }
        }

        private async Task<bool> ValidateFieldsAsync()
        {
            bool success = true;

            if (string.IsNullOrEmpty(Mobile))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.MobileEmpty, Constants.OK);
                success = false;
            }
            else if (string.IsNullOrEmpty(ContactPersonName))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.contactPersonNameEmpty, Constants.OK);
                success = false;
            }
            else if (string.IsNullOrEmpty(Email))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.EmailEmpty, Constants.OK);
                success = false;
            }
            else if (!Regex.IsMatch(Mobile, @"^[0-9]{10}$", RegexOptions.IgnoreCase))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.MobileInvalid, Constants.OK);
                success = false;
            }
            else if (!Regex.IsMatch(Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.EmailInvalid, Constants.OK);
                success = false;
            }
            else if (!Regex.IsMatch(billingGST, @"^[a-z0-9]{15}$", RegexOptions.IgnoreCase))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.GSTInvalid, Constants.OK);
                success = false;
            }

            return success;
        }
    }
}
