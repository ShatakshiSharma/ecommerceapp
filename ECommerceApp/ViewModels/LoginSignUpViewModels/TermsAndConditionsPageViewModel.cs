﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace ECommerceApp.ViewModels.LoginSignUpViewModels
{
    public class TermsAndConditionsPageViewModel : BaseViewModel
    {
        public ICommand CancelCommand { get; set; }

        public TermsAndConditionsPageViewModel()
        {
            CancelCommand = new Command(CancelAction);
        }

        private async void CancelAction()
        {
            await Application.Current.MainPage.Navigation.PopModalAsync();
        }
    }
}
