﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ECommerceApp.Interfaces;
using Xamarin.Forms;
using ECommerceApp.Views.MyCart;
using System.Windows.Input;
using ECommerceApp.Views.LoginSignup;
using ECommerceApp.Models;
using ECommerceApp.DataAccess;
using Newtonsoft.Json;

namespace ECommerceApp.ViewModels.MyCartViewModels
{
    public class MyCartViewModel : BaseViewModel
    {
        private LoginDataAccess _loginDataAccess = new LoginDataAccess();

        public ICommand CheckoutCommand { get; set; }
        public ICommand ConfirmCommand { get; set; }

        private List<MyCartModel> _productList = new List<MyCartModel>();
        public List<MyCartModel> ProductList
        {
            get { return _productList; }
            set { SetProperty(ref _productList, value); }
        }

        private string _name = Global.LoggedInUser.user_display_name;
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        private string _address = "";
        public string Address
        {
            get { return _address; }
            set { SetProperty(ref _address, value); }
        }

        private bool _listViewIsVisible = true;
        public bool ListViewIsVisible
        {
            get { return _listViewIsVisible; }
            set { SetProperty(ref _listViewIsVisible, value); }
        }

        private bool _labelIsVisible = false;
        public bool LabelIsVisible
        {
            get { return _labelIsVisible; }
            set { SetProperty(ref _labelIsVisible, value); }
        }

        private string _labelMessage = Constants.EmptyCart;
        public string LabelMessage
        {
            get { return _labelMessage; }
            set { SetProperty(ref _labelMessage, value); }
        }

        private string _total = "";
        public string Total
        {
            get { return _total; }
            set { SetProperty(ref _total, value); }
        }

        public MyCartViewModel()
        {
            CheckoutCommand = new Command(CheckoutAction);
            ConfirmCommand = new Command(ConfirmAction);

            if (Global.Cart != null)
            {
                ListViewIsVisible = true;
                LabelIsVisible = false;
                ProductList = Global.Cart;
                Total = Global.Cart[0].displayTotal;
            }
            else
            {
                ListViewIsVisible = false;
                LabelIsVisible = true;
            }

            Address = Global.UserDetail.data.shipping.address_1 + ", " + Global.UserDetail.data.shipping.address_2 + ", " + Global.UserDetail.data.shipping.city + ", " + Global.UserDetail.data.shipping.state + ", " + Global.UserDetail.data.shipping.country + " - " + Global.UserDetail.data.shipping.postcode;

            OnPropertyChanged(nameof(Address));
            OnPropertyChanged(nameof(ListViewIsVisible));
            OnPropertyChanged(nameof(LabelIsVisible));
            OnPropertyChanged(nameof(ProductList));
            OnPropertyChanged(nameof(Total));
        }

        public async Task RemoveItem(int itemIndex)
        {
            string confirm = await App.Current.MainPage.DisplayActionSheet("Are you sure you want to remove?", null, null, new string[] { "Yes", "No" });

            if (confirm == "Yes")
            {
                try
                {
                    ProductList.RemoveAt(itemIndex);
                    Helpers.SettingsHelper.Cart = JsonConvert.SerializeObject(ProductList);
                    Global.Cart = JsonConvert.DeserializeObject<List<MyCartModel>>(Helpers.SettingsHelper.Cart);

                    if (Global.Cart != null && Global.Cart.Count > 0)
                    {
                        ListViewIsVisible = true;
                        LabelIsVisible = false;
                        ProductList = Global.Cart;
                        Total = Global.Cart[0].displayTotal;
                    }
                    else
                    {
                        ListViewIsVisible = false;
                        LabelIsVisible = true;
                    }
                    OnPropertyChanged(nameof(ListViewIsVisible));
                    OnPropertyChanged(nameof(LabelIsVisible));
                    OnPropertyChanged(nameof(ProductList));
                    OnPropertyChanged(nameof(Total));
                }
                catch (Exception exc)
                {
                }
            }
         
        }


        private async void CheckoutAction()
        {
            CheckoutPage obj = new CheckoutPage();
            await Application.Current.MainPage.Navigation.PushModalAsync(obj);
        }

        private async void ConfirmAction()
        {
            await SendOTPAsync();
        }

        [Obsolete]
        public async Task SendOTPAsync()
        {
            try
            {
                DependencyService.Get<ILoaderInterface>().Show("Wait . . .");
                string otp = await _loginDataAccess.CallPostOTPMobileAPI(Global.LoggedInUser.mobile_number);
                DependencyService.Get<ILoaderInterface>().Hide();
                if (otp != null && otp != "")
                {
                    var cartArray = new List<Dictionary<string, string>>();

                    foreach (var product in ProductList)
                    {
                        Dictionary<string, string> dict = new Dictionary<string, string>();
                        dict.Add("product_id", product.product_id);
                        dict.Add("quantity", product.quantity);

                        cartArray.Add(dict);
                    }
                    OtpVerificationPage obj = new OtpVerificationPage(otp, cartArray);
                    await Application.Current.MainPage.Navigation.PushModalAsync(obj);
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.OTPError, Constants.OK);
                }
            }
            catch (Exception exc)
            {
                // DependencyService.Get<Helpers.LoggerHelper>().LogException(exc);
            }
        }
    }
}

