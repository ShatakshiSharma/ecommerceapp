﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace ECommerceApp.ViewModels.NotificationViewModel
{
    public class NotificationViewModel : BaseViewModel
    {
        private List<string> _notificationList = new List<string>() { "Your order for saree has been dispatched", "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. A enean Lorem", "Lorem ipsum dolor sit amet, consectetuer adipiscing elit." };
        public List<string> NotificationList
        {
            get { return _notificationList; }
            set { SetProperty(ref _notificationList, value); }
        }

        public NotificationViewModel()
        {
            
        }
    }
}

