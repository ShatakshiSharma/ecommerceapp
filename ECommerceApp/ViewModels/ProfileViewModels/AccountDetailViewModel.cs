﻿using System;
using System.Windows.Input;
using ECommerceApp.Views.Profile;
using Xamarin.Forms;
using System.Threading.Tasks;
using ECommerceApp.DataAccess;
using ECommerceApp.Helpers;
using ECommerceApp.Interfaces;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ECommerceApp.Interfaces;
using RestSharp;

namespace ECommerceApp.ViewModels.ProfileViewModels
{
    public class AccountDetailViewModel : BaseViewModel
    {
        public ICommand EditCommand { get; set; }
        public ICommand SubmitCommand { get; set; }
        private SignUpDataAccess _signUpDataAccess = new SignUpDataAccess();

        private string firmName = Global.LoggedInUser.firm_name;
        private string contactPersonName = Global.UserDetail.data.first_name;
        private string mobile = Global.LoggedInUser.mobile_number;
        private string city = "";
        private string email = Global.UserDetail.data.email;
        private string billingAddress = Global.UserDetail.data.billing.address_1;
        private string billingArea = Global.UserDetail.data.billing.address_2;
        private string billingState = Global.UserDetail.data.billing.state;
        private string billingCity = Global.UserDetail.data.billing.city;
        private string billingPincode = Global.UserDetail.data.billing.postcode;
        private string billingGST = "";
        private string shippingAddress = Global.UserDetail.data.shipping.address_1;
        private string shippingArea = Global.UserDetail.data.shipping.address_2;
        private string shippingState = Global.UserDetail.data.shipping.state;
        private string shippingCity = Global.UserDetail.data.shipping.city;
        private string shippingPincode = Global.UserDetail.data.shipping.postcode;

        public string FirmName
        {
            get
            {
                return firmName;
            }
            set
            {
                SetProperty(ref firmName, value);
            }
        }

        public string ContactPersonName
        {
            get
            {
                return contactPersonName;
            }
            set
            {
                SetProperty(ref contactPersonName, value);
            }
        }

        public string Mobile
        {
            get
            {
                return mobile;
            }
            set
            {
                SetProperty(ref mobile, value);
            }
        }

        public string City
        {
            get
            {
                return city;
            }
            set
            {
                SetProperty(ref city, value);
            }
        }

        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                SetProperty(ref email, value);
            }
        }

        public string BillingAddress
        {
            get
            {
                return billingAddress;
            }
            set
            {
                SetProperty(ref billingAddress, value);
            }
        }

        public string BillingArea
        {
            get
            {
                return billingArea;
            }
            set
            {
                SetProperty(ref billingArea, value);
            }
        }

        public string BillingState
        {
            get
            {
                return billingState;
            }
            set
            {
                SetProperty(ref billingState, value);
            }
        }

        public string BillingCity
        {
            get
            {
                return billingCity;
            }
            set
            {
                SetProperty(ref billingCity, value);
            }
        }

        public string BillingPincode
        {
            get
            {
                return billingPincode;
            }
            set
            {
                SetProperty(ref billingPincode, value);
            }
        }

        public string BillingGST
        {
            get
            {
                return billingGST;
            }
            set
            {
                SetProperty(ref billingGST, value);
            }
        }

        public string ShippingAddress
        {
            get
            {
                return shippingAddress;
            }
            set
            {
                SetProperty(ref shippingAddress, value);
            }
        }

        public string ShippingArea
        {
            get
            {
                return shippingArea;
            }
            set
            {
                SetProperty(ref shippingArea, value);
            }
        }

        public string ShippingState
        {
            get
            {
                return shippingState;
            }
            set
            {
                SetProperty(ref shippingState, value);
            }
        }

        public string ShippingCity
        {
            get
            {
                return shippingCity;
            }
            set
            {
                SetProperty(ref shippingCity, value);
            }
        }

        public string ShippingPincode
        {
            get
            {
                return shippingPincode;
            }
            set
            {
                SetProperty(ref shippingPincode, value);
            }
        }

        public AccountDetailViewModel()
        {
            EditCommand = new Command(EditAction);
            SubmitCommand = new Command(SubmitAction);

            foreach (var attribute in Global.UserDetail.data.meta_data)
            {
                if (attribute.key == "gst_number")
                {
                    BillingGST = attribute.value;
                }
            }
            OnPropertyChanged(nameof(BillingGST));
        }

        private async void EditAction()
        {
            EditAccountDetailPage obj = new EditAccountDetailPage();
            await Application.Current.MainPage.Navigation.PushModalAsync(obj);
        }

        [Obsolete]
        private async void SubmitAction()
        {
            await SignUpAsync();
        }
        [Obsolete]
        public async Task SignUpAsync()
        {
            if (await ValidateFieldsAsync())
            {
                try
                {
                    DependencyService.Get<ILoaderInterface>().Show("Wait . . .");

                    Dictionary<string, object> billingDict = new Dictionary<string, object>();
                    billingDict.Add("address_1", BillingAddress);
                    billingDict.Add("address_2", BillingArea);
                    billingDict.Add("state", BillingState);
                    billingDict.Add("city", BillingCity);
                    billingDict.Add("postcode", BillingPincode);
                    billingDict.Add("first_name", Global.UserDetail.data.first_name);
                    billingDict.Add("last_name", Global.UserDetail.data.last_name);
                    billingDict.Add("company", Global.UserDetail.data.billing.company);
                    billingDict.Add("country", Global.UserDetail.data.billing.country);
                    billingDict.Add("phone", Global.LoggedInUser.mobile_number);
                    billingDict.Add("email", Global.LoggedInUser.user_email);

                    Dictionary<string, object> shippingDict = new Dictionary<string, object>();
                    shippingDict.Add("address_1", shippingAddress);
                    shippingDict.Add("address_2", shippingArea);
                    shippingDict.Add("state", shippingState);
                    shippingDict.Add("city", shippingCity);
                    shippingDict.Add("postcode", shippingPincode);
                    shippingDict.Add("first_name", Global.UserDetail.data.first_name);
                    shippingDict.Add("last_name", Global.UserDetail.data.last_name);
                    shippingDict.Add("company", Global.UserDetail.data.shipping.company);
                    shippingDict.Add("country", Global.UserDetail.data.shipping.country);
                    shippingDict.Add("phone", Global.LoggedInUser.mobile_number);

                    JsonObject billingObject = new JsonObject();
                    foreach (var kvp in billingDict)
                    {
                        billingObject.Add(kvp);
                    }

                    JsonObject shippingObject = new JsonObject();
                    foreach (var kvp in shippingDict)
                    {
                        shippingObject.Add(kvp);
                    }

                    Dictionary<string, dynamic> dictDict = new Dictionary<string, dynamic>();
                    dictDict.Add("token", Helpers.SettingsHelper.SavedToken);
                    dictDict.Add("name", ContactPersonName);
                    dictDict.Add("email", Email);
                    dictDict.Add("gst_number", BillingGST);
                    dictDict.Add("billing", billingObject.ToString());
                    dictDict.Add("shipping", shippingObject.ToString());

                    bool isSuccess = await _signUpDataAccess.CallPostUpdateUserAPI(dictDict);
                    DependencyService.Get<ILoaderInterface>().Hide();
                    if (isSuccess)
                    {
                        Helpers.SettingsHelper.IsUserLoggedIn = true;
                        await Xamarin.Forms.Application.Current.MainPage.Navigation.PopModalAsync();
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.LoginError, Constants.OK);
                    }
                }
                catch (Exception exc)
                {
                    DependencyService.Get<ILoaderInterface>().Hide();

                    // DependencyService.Get<Helpers.LoggerHelper>().LogException(exc);
                }
            }
        }

        private async Task<bool> ValidateFieldsAsync()
        {
            bool success = true;

            if (string.IsNullOrEmpty(Mobile))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.MobileEmpty, Constants.OK);
                success = false;
            }
            else if (string.IsNullOrEmpty(ContactPersonName))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.contactPersonNameEmpty, Constants.OK);
                success = false;
            }
            else if (string.IsNullOrEmpty(Email))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.EmailEmpty, Constants.OK);
                success = false;
            }
            else if (!Regex.IsMatch(Mobile, @"^[0-9]{10}$", RegexOptions.IgnoreCase))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.MobileInvalid, Constants.OK);
                success = false;
            }
            else if (!Regex.IsMatch(Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.EmailInvalid, Constants.OK);
                success = false;
            }
            else if (!Regex.IsMatch(billingGST, @"^[a-z0-9]{15}$", RegexOptions.IgnoreCase))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.GSTInvalid, Constants.OK);
                success = false;
            }

            return success;
        }
    }
}

