﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ECommerceApp.DataAccess;
using ECommerceApp.Interfaces;
using ECommerceApp.Models;
using Xamarin.Forms;

namespace ECommerceApp.ViewModels.ProfileViewModels
{
    public class OrderViewModel : BaseViewModel
    {
        private OrderDataAccess _homeDataAccess = new OrderDataAccess();

        private List<OrderArray> _orderList = new List<OrderArray>();
        public List<OrderArray> OrderList
        {
            get { return _orderList; }
            set
            {
                _orderList = value;
                OnPropertyChanged(nameof(_orderList));
            }
        }

        private bool _listViewIsVisible = true;
        public bool ListViewIsVisible
        {
            get { return _listViewIsVisible; }
            set { SetProperty(ref _listViewIsVisible, value); }
        }

        private bool _labelIsVisible = false;
        public bool LabelIsVisible
        {
            get { return _labelIsVisible; }
            set { SetProperty(ref _labelIsVisible, value); }
        }

        private string _labelMessage = Constants.NoDataFound;
        public string LabelMessage
        {
            get { return _labelMessage; }
            set { SetProperty(ref _labelMessage, value); }
        }

        public OrderViewModel()
        {
        }

        public async Task GetOrderListAsync()
        {
            try
            {
                DependencyService.Get<ILoaderInterface>().Show("Wait . . .");
                OrderList = await OrderDataAccess.CallGetOrderListAPI();
               
                if (OrderList == null || OrderList.Count == 0)
                {
                    ListViewIsVisible = false;
                    LabelIsVisible = true;
                 
                }
                else
                {
                    foreach (var order in OrderList)
                    {
                        if (order.line_items != null && order.line_items.Count > 0)
                        {
                            order.name = order.line_items[0].name;
                            order.quantity = order.line_items[0].DisplayQuantity;
                        }
                        else
                        {
                            order.name = "Order name";
                            order.quantity = "Qty: ";
                        }
                    }
                    ListViewIsVisible = true;
                    LabelIsVisible = false;
                }
                OnPropertyChanged(nameof(OrderList));
                DependencyService.Get<ILoaderInterface>().Hide();
            }
            catch (Exception exc)
            {
                // DependencyService.Get<Helpers.LoggerHelper>().LogException(exc);
            }
        }

    }
}

