﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using ECommerceApp.Views.Profile;

namespace ECommerceApp.ViewModels.ProfileViewModels
{
    public class ProfileViewModel : BaseViewModel
    {
        public ICommand AccountDetailCommand { get; set; }
        public ICommand OrderHistoryCommand { get; set; }
        public ICommand SupportCommand { get; set; }
        public ICommand AboutCommand { get; set; }
        public ICommand LogoutCommand { get; set; }

        private string firmName = "Hello, " + Global.LoggedInUser.firm_name;
        public string FirmName
        {
            get
            {
                return firmName;
            }
            set
            {
                SetProperty(ref firmName, value);
            }
        }

        private string image = "";
        public string Image
        {
            get
            {
                return image;
            }
            set
            {
                SetProperty(ref image, value);
            }
        }

   
        public ProfileViewModel()
        {
            AccountDetailCommand = new Command(AccountDetailAction);
            AboutCommand = new Command(AboutAction);
            SupportCommand = new Command(SupportAction);
            OrderHistoryCommand = new Command(OrderHistoryAction);
            LogoutCommand = new Command(LogoutAction);
        }

        private async void OrderHistoryAction()
        {
            OrderHistoryPage obj = new OrderHistoryPage();
            await Application.Current.MainPage.Navigation.PushModalAsync(obj);
        }

        private async void AccountDetailAction()
        {
            AccountDetailPage obj = new AccountDetailPage();
            await Application.Current.MainPage.Navigation.PushModalAsync(obj);
        }

        private async void AboutAction()
        {
            AboutAppPage obj = new AboutAppPage();
            await Application.Current.MainPage.Navigation.PushModalAsync(obj);
        }

        private async void SupportAction()
        {
            SupportPage obj = new SupportPage();
            await Application.Current.MainPage.Navigation.PushModalAsync(obj);
        }

        private async void LogoutAction()
        {
        }
    }
}

