﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using ECommerceApp.Interfaces;
using Xamarin.Forms;

namespace ECommerceApp.ViewModels.ProfileViewModels
{
    public class SupportViewModel : BaseViewModel
    {
        public ICommand CancelCommand { get; set; }
        public ICommand SaveCommand { get; set; }

        private string email = "";
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                SetProperty(ref email, value);
            }
        }

        private string description = "";
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                SetProperty(ref description, value);
            }
        }

        public SupportViewModel()
        {
            CancelCommand = new Command(CancelAction);
            SaveCommand = new Command(SaveAction);
        }

        private async void CancelAction()
        {
            Email = "";
            Description = "";

            OnPropertyChanged(nameof(Email));
            OnPropertyChanged(nameof(Description));

        }

        private async void SaveAction()
        {
            if (await ValidateFieldsAsync())
            {
                DependencyService.Get<ILoaderInterface>().Show("Wait . . .");

                await Task.Delay(1000);

                DependencyService.Get<ILoaderInterface>().Hide();
                Email = "";
                Description = "";

                OnPropertyChanged(nameof(Email));
                OnPropertyChanged(nameof(Description));
            }
        }

        private async Task<bool> ValidateFieldsAsync()
        {
            bool success = true;

            if (string.IsNullOrEmpty(Email))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.EmailEmpty, Constants.OK);
                success = false;
            }
            if (string.IsNullOrEmpty(Description))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.DescriptionEmpty, Constants.OK);
                success = false;
            }
            else if (!Regex.IsMatch(Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                await Application.Current.MainPage.DisplayAlert(Constants.Error, Constants.EmailInvalid, Constants.OK);
                success = false;
            }

            return success;
        }
    }
}

