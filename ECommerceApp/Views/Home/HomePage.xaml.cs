﻿using System;
using System.Collections.Generic;
using ECommerceApp.Models;
using ECommerceApp.ViewModels.HomeViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Views.Home
{
    public partial class HomePage : ContentPage
    {
        HomeViewModel viewModel { get; set; }

        public HomePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            BindingContext = viewModel = new HomeViewModel();
            viewModel.GetcategoryListAsync();

        }

        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        async void CategoryCollectionView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var previous = e.PreviousSelection;

            try
            {
                if (e.CurrentSelection == null || e.CurrentSelection.Count == 0)
                {
                    return;
                }
                var selectedItem = (e.CurrentSelection[0] as CategoryList);
                (sender as CollectionView).SelectedItem = null;
                viewModel.SubCategoryList = new List<CategoryList>();

                for (int i = 0; i < viewModel.CompleteCategoryList.Count; i++)
                {
                    if (selectedItem.id == viewModel.CompleteCategoryList[i].parent)
                    {
                        viewModel.SubCategoryList.Add(viewModel.CompleteCategoryList[i]);
                    }
                }
                if (viewModel.SubCategoryList != null && viewModel.SubCategoryList.Count > 0)
                {
                    await viewModel.GoToSubCategoryScreen(viewModel.SubCategoryList, selectedItem.name);
                }
                else
                {
                    await viewModel.GoToProductListingScreen(selectedItem.id, selectedItem.name);
                }
            }
            catch (Exception exc)
            {
            }
        }
    }
}
