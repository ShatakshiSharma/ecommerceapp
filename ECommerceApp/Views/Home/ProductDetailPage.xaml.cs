﻿using ECommerceApp.Models;
using ECommerceApp.ViewModels.HomeViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Views.Home
{
    public partial class ProductDetailPage : ContentPage
    {
        ProductDetailViewModel viewModel { get; set; }

        public ProductDetailPage(ProductDetailModel productDetailModel)
        {
            InitializeComponent();
            BindingContext = viewModel = new ProductDetailViewModel();
            viewModel.Title = productDetailModel.Name;
            viewModel.ProductList = productDetailModel;
            viewModel.GetProductDetailtAsync();
        }

        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        async void Handle_PositionSelected(object sender, CarouselView.FormsPlugin.Abstractions.PositionSelectedEventArgs e)
        {
            
        }

        void ProductCollectionView_SelectionChanged(System.Object sender, Xamarin.Forms.SelectionChangedEventArgs e)
        {
        }

        async void AddQuantityButton_Clicked(System.Object sender, System.EventArgs e)
        {
            string result = await DisplayPromptAsync("Please enter quantity of the product", "", placeholder: "Enter quantity");
            viewModel.Quantity = result;
            await viewModel.AddQuantity();
        }


    }
}
