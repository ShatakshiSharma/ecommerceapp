﻿using System;
using System.Collections.Generic;
using ECommerceApp.Models;
using ECommerceApp.ViewModels.HomeViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Views.Home
{
    public partial class ProductListingPage : ContentPage
    {
        ProductListingViewModel viewModel { get; set; }

        public ProductListingPage(string id, string title)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            BindingContext = viewModel = new ProductListingViewModel();
            viewModel.Title = title;
            viewModel.ID = id;

        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            if (Device.RuntimePlatform == Device.iOS)
            {
                searchBar.Focus();
                searchBar.Unfocus();
            }
            await viewModel.GetProductListAsync();

        }

        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        async void ProductListingCollectionView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var previous = e.PreviousSelection;

            try
            {
                if (e.CurrentSelection == null || e.CurrentSelection.Count == 0)
                {
                    return;
                }
                var selectedItem = (e.CurrentSelection[0] as ProductDetailModel);
                (sender as CollectionView).SelectedItem = null;

                await viewModel.GoToProductDetailScreen(selectedItem);

            }
            catch (Exception exc)
            {
            }
         
        }

        void filter_Clicked(System.Object sender, System.EventArgs e)
        {
        }

        private async void SearchBar_TextChanged(object sender, TextChangedEventArgs textChangedEventArgs)
        {

        }
    }
}
