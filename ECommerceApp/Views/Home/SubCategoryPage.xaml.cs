﻿using System;
using System.Collections.Generic;
using ECommerceApp.Models;
using ECommerceApp.ViewModels.HomeViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Views.Home
{
    public partial class SubCategoryPage : ContentPage
    {
        HomeViewModel viewModel { get; set; }

        public SubCategoryPage(List<CategoryList> subcategory, string title)
        {
            InitializeComponent();
            BindingContext = viewModel = new HomeViewModel();
            viewModel.SubCategoryList = subcategory;
            viewModel.Title = title;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (Device.RuntimePlatform == Device.iOS)
            {
                searchBar.Focus();
                searchBar.Unfocus();
            }
        }

        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        private async void SearchBar_TextChanged(object sender, TextChangedEventArgs textChangedEventArgs)
        {
            
        }

        async void CategoryListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;

            CategoryList selectedCategory = e.Item as CategoryList;

            //Deselect Item
            ((ListView)sender).SelectedItem = null;

            (sender as ListView).IsEnabled = false;
            await viewModel.GoToProductListingScreen(selectedCategory.id, selectedCategory.name);

        }
    }
}
