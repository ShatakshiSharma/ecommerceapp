﻿using System;
using System.Collections.Generic;
using ECommerceApp.ViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Views
{
    public partial class FirstLandingPage : ContentPage
    {
        FirstlandingPageViewModel viewModel { get; set; }

        public FirstLandingPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new FirstlandingPageViewModel();

        }
    }
}
