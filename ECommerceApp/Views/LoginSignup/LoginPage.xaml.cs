﻿using System;
using System.Collections.Generic;
using ECommerceApp.ViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Views
{
    public partial class LoginPage : ContentPage
    {
        LoginPageViewModel viewModel { get; set; }

        public LoginPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new LoginPageViewModel();

        }

    }
}
