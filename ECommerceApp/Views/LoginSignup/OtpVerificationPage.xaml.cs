﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ECommerceApp.ViewModels.LoginSignUpViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Views.LoginSignup
{
    public partial class OtpVerificationPage : ContentPage
    {
        private string otp;

        OtpVerificationPageViewModel viewModel { get; set; }

        public OtpVerificationPage(string otp, List<Dictionary<string, string>> cartArray)
        {
            InitializeComponent();
            BindingContext = viewModel = new OtpVerificationPageViewModel(false);
            viewModel.OTPResponse = otp;
            viewModel.CartArray = cartArray;
        }

        public OtpVerificationPage(string otp)
        {
            InitializeComponent();
            BindingContext = viewModel = new OtpVerificationPageViewModel(true);
            viewModel.OTPResponse = otp;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                Entry1.Focus();
                return false; // True = Repeat again, False = Stop the timer
            });
        }
        protected override void OnDisappearing()
        {
            Entry1.Unfocus();
            Entry2.Unfocus();
            Entry3.Unfocus();
            Entry4.Unfocus();
        }

        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        void Entry1_TextChanged(System.Object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            try
            {
                Entry entry = (Entry)sender;
                string input = e.NewTextValue;
                if (!Regex.IsMatch(e.NewTextValue, "^[0-9]+$", RegexOptions.CultureInvariant))
                {
                    entry.Text = Regex.Replace(e.NewTextValue, "[^0-9]", string.Empty);
                }
                if (entry.Text.Length == 1)
                {
                    Entry1.Unfocus();
                    Entry2.Focus();
                }
                viewModel.OTP = Entry1.Text + Entry2.Text + Entry3.Text + Entry4.Text;
            }
            catch
            {

            }
        }

        void Entry2_TextChanged(System.Object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            try
            {
                Entry entry = (Entry)sender;
                string input = e.NewTextValue;
                if (!Regex.IsMatch(e.NewTextValue, "^[0-9]+$", RegexOptions.CultureInvariant))
                {
                    entry.Text = Regex.Replace(e.NewTextValue, "[^0-9]", string.Empty);
                }
                if (entry.Text.Length == 1)
                {
                    Entry2.Unfocus();
                    Entry3.Focus();
                }
                viewModel.OTP = Entry1.Text + Entry2.Text + Entry3.Text + Entry4.Text;
            }
            catch
            {

            }
        }

        void Entry3_TextChanged(System.Object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            try
            {
                Entry entry = (Entry)sender;
                string input = e.NewTextValue;
                if (!Regex.IsMatch(e.NewTextValue, "^[0-9]+$", RegexOptions.CultureInvariant))
                {
                    entry.Text = Regex.Replace(e.NewTextValue, "[^0-9]", string.Empty);
                }
                if (entry.Text.Length == 1)
                {
                    Entry3.Unfocus();
                    Entry4.Focus();
                }
                viewModel.OTP = Entry1.Text + Entry2.Text + Entry3.Text + Entry4.Text;
            }
            catch
            {

            }
        }

        void Entry4_TextChanged(System.Object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            try
            {
                Entry entry = (Entry)sender;
                string input = e.NewTextValue;
                if (!Regex.IsMatch(e.NewTextValue, "^[0-9]+$", RegexOptions.CultureInvariant))
                {
                    entry.Text = Regex.Replace(e.NewTextValue, "[^0-9]", string.Empty);
                }
                if (entry.Text.Length == 1)
                {
                    Entry4.Unfocus();
                }
                viewModel.OTP = Entry1.Text + Entry2.Text + Entry3.Text + Entry4.Text;
            }
            catch
            {

            }
        }
    }
}
