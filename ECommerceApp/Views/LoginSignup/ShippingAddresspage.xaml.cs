﻿using System;
using System.Collections.Generic;
using ECommerceApp.ViewModels.LoginSignUpViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Views.LoginSignup
{
    public partial class ShippingAddresspage : ContentPage
    {
        SignUpPageViewModel viewModel { get; set; }

        public ShippingAddresspage()
        {
            InitializeComponent();
            BindingContext = viewModel = new SignUpPageViewModel();

        }

        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync();
        }
    }
}
