﻿using System;
using System.Collections.Generic;
using ECommerceApp.ViewModels.LoginSignUpViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Views.LoginSignup
{
    public partial class SignUpPage : ContentPage
    {
        SignUpPageViewModel viewModel { get; set; }

        public SignUpPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new SignUpPageViewModel();
        }
    }
}
