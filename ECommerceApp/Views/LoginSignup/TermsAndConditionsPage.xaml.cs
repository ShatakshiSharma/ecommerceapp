﻿using System;
using System.Collections.Generic;
using ECommerceApp.ViewModels.LoginSignUpViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Views.LoginSignup
{
    public partial class TermsAndConditionsPage : ContentPage
    {
        TermsAndConditionsPageViewModel viewModel { get; set; }

        public TermsAndConditionsPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new TermsAndConditionsPageViewModel();

        }
    }
}
