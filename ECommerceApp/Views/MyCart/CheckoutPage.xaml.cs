﻿using System;
using System.Collections.Generic;
using ECommerceApp.ViewModels.MyCartViewModels;

using Xamarin.Forms;

namespace ECommerceApp.Views.MyCart
{
    public partial class CheckoutPage : ContentPage
    {
        MyCartViewModel viewModel { get; set; }

        public CheckoutPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            BindingContext = viewModel = new MyCartViewModel();

            if (Global.Cart != null)
            {
                CartListView.HeightRequest = Global.Cart.Count * 140;
            }
            else
            {
                CartListView.HeightRequest = 0;
            }
        }

        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        void CartListView_ItemTapped(System.Object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
        }
    }
}
