﻿using System;
using System.Collections.Generic;
using ECommerceApp.ViewModels.MyCartViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Views.MyCart
{
    public partial class MyCartPage : ContentPage
    {
        MyCartViewModel viewModel { get; set; }

        public MyCartPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            BindingContext = viewModel = new MyCartViewModel();

            if (Global.Cart != null)
            {
                CartListView.HeightRequest = Global.Cart.Count * 140;
            }
            else
            {
                CartListView.HeightRequest = 0;
            }
        }

        async void CartListView_ItemTapped(System.Object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;

            //Deselect Item
            ((ListView)sender).SelectedItem = null;

            (sender as ListView).IsEnabled = false;
            await viewModel.RemoveItem(e.ItemIndex);
        }
    }
}
