﻿using System;
using System.Collections.Generic;
using ECommerceApp.Views.Home;
using ECommerceApp.Views.MyCart;
using ECommerceApp.Views.Notifications;
using ECommerceApp.Views.Profile;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Xamarin.Forms.Xaml;

namespace ECommerceApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class NavPage : Xamarin.Forms.TabbedPage
    {
        public NavPage()
        {
            InitializeComponent();
            this.On<Xamarin.Forms.PlatformConfiguration.Android>().SetIsSwipePagingEnabled(false);
            On<Xamarin.Forms.PlatformConfiguration.Android>().SetToolbarPlacement(ToolbarPlacement.Bottom);

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetBackButtonTitle(this, "");

            Global.page1 = new NavigationPage(new HomePage()) { Title = "Home", IconImageSource = "Home.png", BarTextColor = Color.Black };
            Global.page2 = new NavigationPage(new MyCartPage()) { Title = "My Cart", IconImageSource = "shoppingcart.png" };
            Global.page3 = new NavigationPage(new NotificationPage()) { Title = "Notification", IconImageSource = "Notification.png" };
            Global.page4 = new NavigationPage(new ProfilePage()) { Title = "Profile", IconImageSource = "Profile.png" };
            this.Children.Add(Global.page1);
            this.Children.Add(Global.page2);
            this.Children.Add(Global.page3);
            this.Children.Add(Global.page4);

            SelectedItem = Children[0];
        }


    }
}
