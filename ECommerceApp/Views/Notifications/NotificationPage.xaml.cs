﻿using System;
using System.Collections.Generic;
using ECommerceApp.ViewModels.NotificationViewModel;
using Xamarin.Forms;

namespace ECommerceApp.Views.Notifications
{
    public partial class NotificationPage : ContentPage
    {
        NotificationViewModel viewModel { get; set; }

        public NotificationPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = viewModel = new NotificationViewModel();
        }
    }
}
