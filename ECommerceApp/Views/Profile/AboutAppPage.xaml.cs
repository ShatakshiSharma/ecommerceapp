﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ECommerceApp.Views.Profile
{
    public partial class AboutAppPage : ContentPage
    {
        public AboutAppPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync();
        }
    }
}
