﻿using System;
using System.Collections.Generic;
using ECommerceApp.ViewModels.ProfileViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Views.Profile
{
    public partial class AccountDetailPage : ContentPage
    {
        AccountDetailViewModel viewModel { get; set; }

        public AccountDetailPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            BindingContext = viewModel = new AccountDetailViewModel();
        }


        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync();
        }
    }
}
