﻿using System;
using System.Collections.Generic;
using ECommerceApp.ViewModels.ProfileViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Views.Profile
{
    public partial class EditAccountDetailPage : ContentPage
    {
        AccountDetailViewModel viewModel { get; set; }

        public EditAccountDetailPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = viewModel = new AccountDetailViewModel();
        }

        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync();
        }
    }
}
