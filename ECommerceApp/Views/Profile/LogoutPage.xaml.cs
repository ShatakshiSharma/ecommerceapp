﻿using System;
using System.Collections.Generic;
using ECommerceApp.Helpers;
using ECommerceApp.ViewModels.ProfileViewModels;
using ECommerceApp.Views.LoginSignup;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace ECommerceApp.Views.Profile
{
    public partial class LogoutPage : PopupPage
    {
        ProfileViewModel viewModel { get; set; }

        public LogoutPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = viewModel = new ProfileViewModel();
        }

        async void ButtonLogout_Clicked(System.Object sender, System.EventArgs e)
        {
            string confirm = await App.Current.MainPage.DisplayActionSheet("Are you sure you want to logout?", null, null, new string[] { "Yes", "No" });

            if (confirm == "Yes")
            {
                try
                {
                    DependencyService.Get<SettingsHelper>().Logout();
                    await PopupNavigation.Instance.PopAsync();
                    await Navigation.PopAsync();
                    Application.Current.MainPage = new NavigationPage(new LoginPage());
                }
                catch (Exception exc)
                {
                }
            }

        }

        async void ButtonCancel_Clicked(System.Object sender, System.EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }
    }
}
