﻿using System;
using System.Collections.Generic;
using ECommerceApp.ViewModels.ProfileViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Views.Profile
{
    public partial class OrderHistoryPage : ContentPage
    {
        OrderViewModel viewModel { get; set; }

        public OrderHistoryPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            BindingContext = viewModel = new OrderViewModel();
            await viewModel.GetOrderListAsync();
            if (viewModel.OrderList != null)
            {
                CartListView.HeightRequest = viewModel.OrderList.Count * 140;
            }
            else
            {
                CartListView.HeightRequest = 0;
            }
        }

        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        void CartListView_ItemTapped(System.Object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
        }
    }
}
