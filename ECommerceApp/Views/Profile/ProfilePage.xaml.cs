﻿using System;
using System.Collections.Generic;
using ECommerceApp.ViewModels.ProfileViewModels;
using Xamarin.Forms;
using Rg.Plugins.Popup.Services;

namespace ECommerceApp.Views.Profile
{
    public partial class ProfilePage : ContentPage
    {
        ProfileViewModel viewModel { get; set; }

        public ProfilePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = viewModel = new ProfileViewModel();

            Console.WriteLine(Global.LoggedInUser.mobile_number);
        }

        async void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            LogoutPage obj = new LogoutPage();
            await PopupNavigation.Instance.PushAsync(obj);
        }
    }
}
