﻿using System;
using System.Collections.Generic;
using ECommerceApp.ViewModels.ProfileViewModels;
using Xamarin.Forms;

namespace ECommerceApp.Views.Profile
{
    public partial class SupportPage : ContentPage
    {
        SupportViewModel viewModel { get; set; }

        public SupportPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = viewModel = new SupportViewModel();

            Email.Focus();
        }

        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync();
        }

      
    }
}
